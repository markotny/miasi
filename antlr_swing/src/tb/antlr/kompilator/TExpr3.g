tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer ifNumer = 0;
}
prog    : (e+=zakres | e+=expr | e+=comp_expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

zakres : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | d+=decl)* {leaveScope();}) -> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)            -> dodaj(e1={$e1.st},e2={$e2.st})
        | ^(MINUS e1=expr e2=expr)            -> odejmij(e1={$e1.st},e2={$e2.st})
        | ^(MUL   e1=expr e2=expr)            -> mnoz(e1={$e1.st},e2={$e2.st})
        | ^(DIV   e1=expr e2=expr)            -> dziel(e1={$e1.st},e2={$e2.st})
        | ^(PODST i1=ID   e2=expr)            -> podst(id={$ID.text},e={$e2.st})
        | ID                                  -> pobierz(id={$ID.text})
        | INT  {numer++;}                     -> int(i={$INT.text},j={numer.toString()})
        | ^(IF ce=comp_expr e1=expr e2=expr?) -> if(ce={$ce.st},e1={$e1.st},e2={$e2.st},nr={ifNumer.toString()}) 
    ;

comp_expr    
        : ^(EQUAL e1=expr e2=expr)      {ifNumer++;} -> rowne(e1={$e1.st},e2={$e2.st},nr={ifNumer.toString()})
        | ^(NOT_EQUAL e1=expr e2=expr)  {ifNumer++;} -> nierowne(e1={$e1.st},e2={$e2.st},nr={ifNumer.toString()})
        | ^(GREATER e1=expr e2=expr)    {ifNumer++;} -> wieksze(e1={$e1.st},e2={$e2.st},nr={ifNumer.toString()})
        | ^(LESS e1=expr e2=expr)       {ifNumer++;} -> mniejsze(e1={$e1.st},e2={$e2.st},nr={ifNumer.toString()})
     ;
    