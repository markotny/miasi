grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;

blok : BEGIN^ (stat | blok)* END!
    ;

stat
    : expr NL -> expr
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    | NL ->
    ;
    
if_stat
    : IF^ comp_expr THEN! expr (ELSE! expr)?
    ;
    
comp_expr
    : expr
      ( EQUAL^ expr
      | NOT_EQUAL^ expr
      | GREATER^ expr
      | LESS^ expr
      )
    ;
    
expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

BEGIN : '{';

END : '}';

IF : 'if';

ELSE : 'else';

THEN : 'then';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

EQUAL
  : '=='
  ;
  
NOT_EQUAL
  : '!='
  ;

GREATER
  : '>'
  ;

LESS
  : '<'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
